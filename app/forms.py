from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired

class AdicionarAtivoForm(FlaskForm):
    nome_do_ativo = StringField('Nome', validators=[DataRequired()])
    valor_unitario_do_ativo = StringField('Valor', validators=[DataRequired()])
    quantidade_do_ativo = StringField('Quantidade', validators=[DataRequired()])
    submit = SubmitField('Adicionar')