from app import app
from app.forms import AdicionarAtivoForm
from flask import render_template, flash, redirect

#bibliotecas de finanças 
import datetime as dt
import matplotlib.pyplot as plt
from matplotlib import style
import pandas as pd
import pandas_datareader.data as web

@app.route('/')
@app.route('/index')
def index():
    user = {'username': 'Gustavo'}
    return render_template('index.html', title='Meus Ativos', user=user)

@app.route('/adicionar',methods=['GET','POST'])
def adicionar():
    form = AdicionarAtivoForm()
    if form.validate_on_submit():
        flash('Ativo adicionado {} '.format(form.nome_do_ativo.data))
        return redirect('/index')
    return render_template('adicionar_ativos.html', title='Adicionar Ativos', form=form)

@app.route('/remover',methods=['GET','POST'])
def remover():
    return render_template('remover_ativos.html', title='Remover Ativos')

@app.route('/detalhes',methods=['GET','POST'])
def detalhes():
    style.use('ggplot')
    start = dt.datetime(2016,1,1)
    end =  dt.datetime(2020,1,1) #ano/mes/dia
    df = web.DataReader('TSLA', 'yahoo',start,end)
    df.to_csv('tsla.csv')
    return render_template('carteira_detalhada.html', title='Carteira Detalhada')
